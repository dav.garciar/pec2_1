package Eurator;

import java.io.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.*;
import java.text.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;


class Eurator extends JFrame implements ActionListener
{	//ATRIBUTOS

	int oo=0;
	int dd;
	JLabel lo, ld;
	JTextField tfo, tfd;
	JComboBox jcbori, jcbdes;
	JPanel p,p1,p2,p3,p10,p01;
	//private Paint pinta;
	int x,y;
	String datos[] = {"elige moneda","    ", // valores al 17-8-2020
		"euros           ","   1.00",
		"*Alemania marco ","   1.95583",
		"*Austria chelin ","  13.7603 ",
		"*Belgica franco ","  40.3399 ",
		"*Dinamarca corona","   7.04",
	 	"*Espa�a peseta  "," 166.386  ",
		"*Finlandia marco","   5.94573",
		"*Francia franco ","   6.55957",
		"*Holanda florin ","   2.20371",
		"*Irlada libra   ","   0.78756",
		"*Italia lira    ","1936.27   ",
		"*Luxenbrg franco","  40.3399 ",
		"*Portugal escud "," 200.482  ",
		"Argentina peso  ","   77.04 ",
		"Australia DÃ³lar ","   1.54",
		"Bahrain Dina    ","   0.5028 ",
		"Brasil Real     ","   1.54 ",
		"Bulgaria Lev    ","   1.73  ",
		"Canada DÃ³lar    ","   1.48  ",
		"China Yuan      ","  7.32 ",
		"Croacia Kuna    ","   6.64 ",
		"Chipre Esterlina","   0.58  ",
		"Eslovaquia Coron","  27.8079",
		"EEUU dolar      ","  1.2113 ",
	 	"Estonia coronas ","  15.6465",
	 	"HongKong dolar  ","  8.34",
		"Hungria florin  "," 307.18",
		"Inglaterra libra","   0.865 ",
		"India rupia   ","  78.30 ",
	 	"Indonesa rupiasi"," 14973,94",
	 	"Islandia coronas","  140.73",
		"Japon  yenes    "," 127.81",
		"Letonia lat     ","   0.7095",
		"LetÃ³nia lita    ","   3.528",
		"Malasia ringgit ","   4.33",
	 	"Noruega coronas ","  9.97 ",
		"Suecia Coronas  ","   9.84 ",
		"Suiza franco    ","   1.03 ",
		"EUSKADI euro    ","   1.0    "
		};
	// array de banderas en el mismo orden del array anterior
	String paises[]={	"Europa.gif",	"Europa.gif   ","Alemania.gif",
		"Austria.gif",	"Belgica.gif",	"Dinamarca.gif","Espa�a.gif",
		"Finlandia.gif","Francia.gif",	"Holanda.gif  ","Ireland.gif",
		"Italy.gif",	"Luxenburgo.gif","Portugal.gif","Argentina.gif",
		"Australia.gif","Bahrain.gif",	"Brasil.gif",	"Bulgaria.gif",
		"Canada.gif",	"Chile.gif"	  ,	"China.gif",	"Croacia.gif",
		"Chipre.gif",	"Eslovaquia.gif","EEUU.gif",
                "Estonia.gif",	"HongKong.gif",	"Hungria.gif",	"Inglaterra.gif",
                "India.gif",    "Indonesia.gif","Islandia.gif",	"Japon.gif",
                "Letonia.gif",  "Lituania.gif",	"Malasia.gif",	"Noruega.gif",
                "Suecia.gif" ,  "Suiza.gif",	"Euzkadi.gif"
		};
	ImageIcon pics[] = new ImageIcon[paises.length];
	ImageIcon cerrar;
	String val[],monedas[];
	String  s[] = {"7","8","9","4","5","6","1","2","3","0",".","C","Off","Borra Todo"};
	JButton b[] = new JButton[s.length];
	JButton banderaOri,banderaDes;
	NumberFormat dc;
 
	//CONSTRUCTOR
	public Eurator()
	{	int i;
		GridLayout g21,g42,g43,g13,g14,g12;
		addWindowListener(wl);
 
		Font f  = new Font("Dialog",Font.BOLD|Font.ITALIC,18);
		Font flabel  = new Font("Dialog",Font.BOLD|Font.ITALIC,15);
		Font ff = new Font("Dialog",Font.BOLD|Font.PLAIN,40);
		monedas = new String[datos.length/2];
		val     = new String[datos.length/2];
 
		for(i=0; i<monedas.length; i++)
		{	monedas[i] = datos[2*i] + " " + datos[2*i+1];	// cargar nombre moneda
			val[i] = datos[2*i+1];							// y su valor en Euros
		}
 
		//carga banderas
		 for (i=0; i < pics.length; i++)
        	pics[i] = new ImageIcon("D:\\JAVA\\eurator\\banderas\\"+paises[i]);
		cerrar = new ImageIcon("banderas\\"+"cerrar.gif");
 
		g21 = new GridLayout(2,1,15,15);
		g13 = new GridLayout(1,3,15,15);
		g42 = new GridLayout(4,2,5,5);
		g43 = new GridLayout(4,3,5,5);
		g14 = new GridLayout(1,4,7,7);
		g12 = new GridLayout(1,2,10,0);
		// etiquetas
		lo = new JLabel("ORiGEN",SwingConstants.CENTER);
		ld = new JLabel("DESTiNO",SwingConstants.CENTER);
		ld.setFont(flabel);
		lo.setFont(flabel);
		//Campos de texto
		tfo = new JTextField("0",20);
		tfd = new JTextField("0",20);
		tfo.setEditable(false);
		tfd.setEditable(false);
		tfo.setFont(f);
		tfd.setFont(f);
		// formato
		dc = NumberFormat.getInstance();
	 	if (dc instanceof DecimalFormat) {
     		((DecimalFormat) dc).setDecimalSeparatorAlwaysShown(true);
     		dc.setMaximumFractionDigits(2);
		}
		// ComboBox
		jcbori= new JComboBox(monedas);// admite cualquier array de Objetos
		jcbori.addActionListener(this);
		jcbori.setEditable(false);
		jcbori.setMaximumRowCount((int)Math.rint(monedas.length/2.5));
		jcbdes= new JComboBox(monedas);
		jcbdes.addActionListener(this);
		jcbdes.setEditable(false);
		jcbdes.setMaximumRowCount((int)Math.rint(monedas.length/2.5));
 
		// Paneles
		p10= new JPanel(g12);
		p01= new JPanel(g12);
		p  = new JPanel(g21);
		p1 = new JPanel(g42);
		p2 = new JPanel(g43);
		p3 = new JPanel(g13);
		p2.setVisible(false);
		getContentPane().add(p);
 
		//Botones
		for(i=0; i < s.length; i++)
		{	b[i] = new JButton(s[i]);
			b[i].setFont(ff);
			b[i].setBackground(Color.black);
			b[i].setForeground(Color.orange);
			b[i].setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED,Color.red,Color.white,new Color(134, 134, 134),new Color(93, 93, 93)));
			b[i].setToolTipText("BOTON "+String.valueOf(i));
			p2.add(b[i]);
			b[i].addActionListener(this);
		}
		b[12].setToolTipText("BOTON APAGAR");
		p2.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.red,Color.white,new Color(134, 134, 134),new Color(93, 93, 93)));
		p1.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED,Color.red,Color.white,new Color(134, 134, 134),new Color(93, 93, 93)));
		banderaOri= new JButton(pics[0]);
		banderaDes= new JButton(pics[0]);
		b[12].setText(null);
		b[12].setBackground (Color.red);
		b[11].setBackground (Color.green);
		b[11].setForeground (Color.black);		b[13].setBackground (Color.red);
		b[12].setFont(f);
		b[12].setIcon(cerrar);
		b[13].setFont(f);
 
		// Campos de texto
		tfo.setBackground(Color.getHSBColor(10,20,21));
		tfd.setBackground(Color.getHSBColor(10,20,21));
		p1.setBackground(Color.green);
		p2.setBackground(Color.gray);
		jcbori.setBackground(Color.getHSBColor(30,30,30));
		jcbdes.setBackground(Color.getHSBColor(30,30,30));
		p3.add(b[12]);
		p10.add(banderaOri);
		p10.add(lo);
		p01.add(banderaDes);
		p01.add(ld);
		p1.add(p10);
		p1.add(p01);
		p1.add(jcbori);
		p1.add(jcbdes);
		p1.add(tfo);
		p1.add(tfd);
		p1.add(b[13]);p1.add(p3);
		p.add(p1);
		p.add(p2);
	}
 
//METODOS
	// Gestion Eventos
	public void actionPerformed(ActionEvent e)
	{	String so,sd;
		//	Boton OFF
		if(e.getSource()==b[12])
			System.exit(-1);
 
		// Lista Origen deplegable
		if(e.getSource()== jcbori)
		{	so=((JComboBox)(e.getSource())).getSelectedItem().toString();
			oo=((JComboBox)(e.getSource())).getSelectedIndex();
			banderaOri.setIcon(pics[oo]);
			lo.setText(paises[oo].substring(0,paises[oo].length()-4));//- .gif
			lo.setToolTipText(paises[oo].substring(0,paises[oo].length()-4));
			if(oo!=0 && dd!=0)
			{	iniciar();//activarTeclado();
				return;
			}
			else
			{	p2.setVisible(false);
				desactivarTeclado();
				return;
			}
		}
 
		// Lista Destino deplegable
		if(e.getSource()== jcbdes)
		{	sd=((JComboBox)(e.getSource())).getSelectedItem().toString();
			dd=((JComboBox)(e.getSource())).getSelectedIndex();
			banderaDes.setIcon(pics[dd]);
			ld.setText(paises[dd].substring(0,paises[dd].length()-4));
			ld.setToolTipText(paises[dd].substring(0,paises[dd].length()-4));
			if(oo!=0 && dd!=0)
			{	iniciar();
				return;
			}
			else
			{	p2.setVisible(false);
				return;
			}
		}
		if(e.getSource() instanceof JButton)
		{
			String ss;
			ss=((JButton)e.getSource()).getText();
			if(ss.charAt(0)>='0' && ss.charAt(0)<='9' || ss.charAt(0)=='.')
			{
				tfo.setText(tfo.getText()+ss.charAt(0));
				tfo.setToolTipText(String.valueOf(dc.format(Double.parseDouble(tfo.getText())))+
				"       "+monedas[jcbori.getSelectedIndex()]+" /  1â‚¬");
				if(b[10]==e.getSource())	b[10].setEnabled(false);
				actualizar();
				return;
			}
			if(ss.charAt(0)=='C')
			{	if(tfo.getText().length()>0)
				{	if(tfo.getText().charAt(tfo.getText().length()-1)=='.')
						b[10].setEnabled(true);
 
					tfo.setText(tfo.getText().substring(0,tfo.getText().length()-1));
 
					b[11].setEnabled(true);
					actualizar();
				}
				if(tfo.getText().length()==0)
					b[11].setEnabled(false);
				return;
			}
			//Borrar Todo
			if(ss.compareTo(b[13].getText())==0)
			{	reiniciar();	return;	}
		}
	}
 
	private void actualizar()
	{
		double valor=0;
		try{valor = Double.parseDouble(tfo.getText()) / Double.parseDouble(val[oo])
					* Double.parseDouble(val[dd]);
			valor=ponDecimales(valor,3);
		}	catch(NumberFormatException nfe){			valor=0;		}
		tfd.setText(String.valueOf(dc.format(valor)));		//salida formateada
		tfd.setToolTipText(String.valueOf(dc.format(valor))+"       "+monedas[jcbdes.getSelectedIndex()]+" /  1â‚¬");
		if(tfo.getText().length()>0) //  activar tecla C
			b[11].setEnabled(true);
	}
 
	private void iniciar()
	{	ActivarTeclado();
		tfo.setText("");
		tfd.setText("");
		b[10].setEnabled(true);
		b[11].setEnabled(false);
	}
 
	private void reiniciar()
	{	iniciar();
		jcbori.setSelectedIndex(0);
		jcbdes.setSelectedIndex(0);
		lo.setText("Origen");
		ld.setText("Destino");
	}
	private void 
	ActivarTeclado()	{	p2.setVisible(true);	}
	private void desactivarTeclado(){	p2.setVisible(false);	}
 
	public static double ponDecimales(double resultado,int precis)
	{	return Math.rint(resultado*Math.pow(10,precis)) / Math.pow(10,precis);
	}
 
	// clase interna
	WindowListener wl = new WindowAdapter() // creamos un objeto  de oyentes cierre de ventana
	{	public void windowClosing(WindowEvent e){
                dispose();System.exit(0);
	}
        };
 
	public static void main(String args[])
	{
                Eurator c = new Eurator();
		c.setTitle("Cambio's Eurator 4.0");
		c.setSize(350,400);
		c.setVisible(true);
		c.setResizable(false);
	}
}
